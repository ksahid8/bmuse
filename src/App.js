import React, { useState } from "react";
import "./App.css";
import Banner from "./Components/Banner/Banner";
import BookSlider from "./Components/BookSlider/BookSlider";
import About from "./Components/About/About";
import Header from "./Layout/Partials/Header/Header";

import { animateScroll as scroll } from "react-scroll";

function App() {
  const [ShowDetails, setShowDetails] = useState(false);
  const [SelectedImage, setSelectedImage] = useState("");
  const [SelectedTitle, setSelectedTitle] = useState("");
  const [SelectedAuthor, setSelectedAuthor] = useState("");
  const [SelectedPublisher, setSelectedPublisher] = useState("");
  const [SelectedDescription, setSelectedDescription] = useState("");

  const ScrollToBottom = () => {
    scroll.scrollToBottom();
  };
  return (
    <>
      <div className="bg-section">
        <Header />
        <Banner />
        <BookSlider
          setSelectedDescription={setSelectedDescription}
          setSelectedImage={setSelectedImage}
          setSelectedTitle={setSelectedTitle}
          setSelectedAuthor={setSelectedAuthor}
          setSelectedPublisher={setSelectedPublisher}
          setShowDetails={setShowDetails}
          ScrollToBottom={ScrollToBottom}
        />
      </div>
      {ShowDetails ? (
        <About
          setShowDetails={setShowDetails}
          image={SelectedImage}
          title={SelectedTitle}
          author={SelectedAuthor}
          description={SelectedDescription}
          publisher={SelectedPublisher}
        />
      ) : null}
    </>
  );
}

export default App;
