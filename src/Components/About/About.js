import React from "react";
import "./About.css";
import LeftSection from "./LeftSection";
import RightSection from "./RightSection";
import CloseButton from "./../../assets/close.png";

const About = (props) => {
  return (
    <div className="about" id="about-section">
      <div className="close-button" onClick={() => props.setShowDetails(false)}>
        <img src={CloseButton} alt="close" />
      </div>
      <LeftSection image={props.image} title={props.title} />
      <RightSection
        publisher={props.publisher}
        author={props.author}
        description={props.description}
      />
    </div>
  );
};
export default About;
