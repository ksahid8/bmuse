import React from 'react'

 const LeftSection = (props) => {
    return (
        <div className="left-section">
            <h1>{props.title}</h1>
            <img src={props.image} className="selected-image"  alt="book" />
            <button>Add to Favorites <strong>+</strong></button>
        </div>
    )
}
export  default LeftSection;