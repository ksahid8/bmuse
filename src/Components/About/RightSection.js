import React from 'react'

 const RightSection = (props) => {
    return (
        <div className="right-section">
            <h5>{props.author} </h5>
            <h6>{props.publisher}</h6>
            <p>{props.description}</p>
        </div>
    )
}
export  default RightSection;