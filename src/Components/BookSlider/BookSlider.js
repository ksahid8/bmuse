import React, { useRef, useState, useEffect } from "react";
import ArrowImageRight from "./../../assets/slider_arrow_right.png";

import ArrowImageLeft from "./../../assets/slider_arrow_left.png";
import SliderCard from "./SliderCard";
import "./BookSlider.css";
import Books from "./books.json";

const BookSlider = (props) => {
  const [ShowRightArrow, setShowRightArrow] = useState(true);
  const [ShowLeftArrow, setShowLeftArrow] = useState(false);

  const SlidersSection = Books.map((key) => {
    return (
      <SliderCard
        key={key.title}
        Image={key.book_image}
        Title={key.title}
        Author={key.author}
        Publisher={key.publisher}
        Description={key.description}
        Rating={key.rating}
        setSelectedDescription={props.setSelectedDescription}
        setSelectedImage={props.setSelectedImage}
        setSelectedTitle={props.setSelectedTitle}
        setSelectedAuthor={props.setSelectedAuthor}
        setSelectedPublisher={props.setSelectedPublisher}
        setShowDetails={props.setShowDetails}
        ScrollToBottom={props.ScrollToBottom}
      />
    );
  });

  const SldierRef = useRef();

  const ScrollToRight = () => {
    SldierRef.current.scrollLeft += 400;
  };

  const ScrollToLeft = () => {
    SldierRef.current.scrollLeft -= 400;
  };

  const [abc, setabc] = useState("book-slider");

  useEffect(() => {
    const handleScroll = () => {
      // setPosition(document.querySelector(".book-slider").scrollLeft);

      if (document.querySelector(".book-slider").scrollLeft > 0) {
        setShowLeftArrow(true);
      } else if (document.querySelector(".book-slider").scrollLeft === 0) {
        setShowLeftArrow(false);
      }

      if (document.querySelector(".book-slider").scrollLeft > 1400) {
        setShowRightArrow(false);
        setabc("book-slider marginright50");
      } else {
        setShowRightArrow(true);
        setabc("book-slider");
      }
    };

    document
      .querySelector(".book-slider")
      .addEventListener("scroll", handleScroll);
  }, []);

  return (
    <>
      <h5 className="slider-header">Lorem Ipsum</h5>
      <div className={abc} ref={SldierRef}>
        {ShowLeftArrow ? (
          <div className="slider-arrow-left" onClick={() => ScrollToLeft()}>
            <img src={ArrowImageLeft} alt="arrow-left" />
          </div>
        ) : null}
        {SlidersSection}
        {ShowRightArrow ? (
          <div className="slider-arrow-right" onClick={() => ScrollToRight()}>
            <img src={ArrowImageRight} alt="arrow-right" />
          </div>
        ) : null}
      </div>
    </>
  );
};
export default BookSlider;
