import React from "react";

const SliderCard = (props) => {
  const ChangeSelectedItem = () => {
    props.setShowDetails(true);
    props.setSelectedImage(props.Image);
    props.setSelectedTitle(props.Title);
    props.setSelectedAuthor(props.Author);
    props.setSelectedDescription(props.Description);
    props.setSelectedPublisher(props.Publisher);
    props.ScrollToBottom();
  };

  return (
    <div className="slider-card" onClick={() => ChangeSelectedItem()}>
      <img src={props.Image} alt="book" />
      <div className="book-overlay"></div>
      <div className="rating">{props.Rating} out of 5</div>
      <div className="title">{props.Title}</div>
      <div className="author">{props.Author}</div>
      <div className="publisher">{props.Publisher}</div>
    </div>
  );
};
export default SliderCard;
