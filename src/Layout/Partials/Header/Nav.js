import React from "react";
import BmuseLogo from "./../../../assets/bmuse_logo.png";
import SearchIcon from "./../../../assets/search.png";

const Nav = () => {
  return (
    <div className="nav">
      <div className="logo-holder">
        <img src={BmuseLogo} alt="bmuse" />
      </div>
      <div className="search-holder">
        <img src={SearchIcon} alt="search" />
      </div>
    </div>
  );
};
export default Nav;
